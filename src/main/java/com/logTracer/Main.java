package com.logTracer;

import com.logTracer.handler.IOHandler;
import com.logTracer.util.ConsoleHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

public class Main {

    private PrintWriter writer = new PrintWriter(System.out);
    private Scanner scanner = new Scanner(System.in);
    private String inputFilePath = null;
    private String outputFilePath = null;

    public static void main(String[] args) {
        int argsNum = args.length;

        if (argsNum >= 1 && needHelp(args[0])) {
            printHelp();
            return;
        }

        if (argsNum > 4) {
            printHelp();
            return;
        }

        Main main = new Main();

        main.setupPaths(args);
        main.setupIn();
        main.setupOut();
        main.printIntro();

        IOHandler IOHandler = new IOHandler(main.scanner, main.writer);
        ConsoleHelper helper = new ConsoleHelper();

        IOHandler.start();

        Instant start = Instant.now();
        if (main.shouldEnableProgress()) {
            printProgressWhileProcessing(IOHandler, helper);
        }

        try {
            IOHandler.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Instant end = Instant.now();
        main.closeOutputIfRequired();
        System.out.println("\nProcessing done! Time taken: " + Duration.between(start, end)); //TODO: nicely format duration

    }

    private void printIntro() {
        if (inputFilePath != null) {
            System.out.println("Start processing file: " + inputFilePath);
        } else {
            System.out.println("Enter logs separated by new line. Type --process when ready to process logs into traces");
        }
    }

    private void closeOutputIfRequired() {
        if (outputFilePath != null) {
            writer.close();
            System.out.println("\n");
            System.out.println("Traces saved to file: " + outputFilePath);
        }
    }

    private boolean shouldEnableProgress() {
        return outputFilePath != null && inputFilePath != null;
    }

    void setupPaths(String[] args) {
        int argsNum = args.length;
        for(int i = 0; i < argsNum; i++) {
            if (args[i].equals("-f") && i + 1 < argsNum) {
                inputFilePath = args[++i];
            } else if (args[i].equals("-o") && i + 1 < argsNum) {
                outputFilePath = args[++i];
            }
        }
    }

    private synchronized void setupOut() {
        if (outputFilePath == null) {
            return;
        }
        try {
            File file = new File(outputFilePath);
            writer = new PrintWriter(new PrintStream(file));
        } catch (FileNotFoundException e) {
            System.err.println("Could not find output file: " + outputFilePath);
        }
    }

    private synchronized void setupIn() {
        if (inputFilePath == null) {
            return;
        }
        try {
            scanner = new Scanner(new File(inputFilePath));
        } catch (FileNotFoundException e) {
            System.err.println("Could not find input file: " + inputFilePath);
        }
    }

    private static void printProgressWhileProcessing(IOHandler IOHandler, ConsoleHelper helper) {
        while (IOHandler.isAlive()) {
            helper.animate(IOHandler.getProcessedLines());
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            IOHandler.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        helper.animate(IOHandler.getProcessedLines());
    }


    private static boolean needHelp(String flag) {
        return flag.equals("-h")
                || flag.equals("--h")
                || flag.equals("help")
                || flag.equals("-help")
                || flag.equals("--help");
    }

    private static void printHelp() {
        System.out.println("NAME\n\tlogTracer -- convert log files into traces\n\n");
        System.out.println("SYNOPSIS\n\tlogTracer [-f input_file] [-o output_file]\n\n");
        System.out.println("DESCRIPTION\n\tlogTracer accepts log files as input and outputs traces as json\n\n");
    }

    String getInputFilePath() {
        return inputFilePath;
    }

    String getOutputFilePath() {
        return outputFilePath;
    }
}
