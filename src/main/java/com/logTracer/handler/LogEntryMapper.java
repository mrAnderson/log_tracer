package com.logTracer.handler;

import com.logTracer.domain.LogEntry;

import java.time.Instant;

public class LogEntryMapper {
    public static LogEntry map(String logString) {
        try {
            String[] params = logString.split("\\s+");
            String trace = params[2];
            String serviceName = params[3];
            Instant start = Instant.parse(params[0]);
            Instant end = Instant.parse(params[1]);
            String fromTo = params[4];
            String[] fromToStr = fromTo.split("->");
            String from = fromToStr[0];
            if ("null".equals(from)) from = null;
            String to = fromToStr[1];
            return new LogEntry(trace, serviceName, from, to, start, end);
        } catch (Exception e) {
            //TODO: count malformations
            return null;
        }
    }
}
