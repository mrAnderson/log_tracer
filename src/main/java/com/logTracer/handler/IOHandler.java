package com.logTracer.handler;

import com.logTracer.application.LogEntryProcessor;
import com.logTracer.domain.LogEntry;
import com.logTracer.domain.PendingEntries;
import com.logTracer.util.Constants;

import java.io.PrintWriter;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class IOHandler extends Thread {
    private final Scanner scanner;
    private final PrintWriter writer;
    private final LogEntryProcessor logEntryProcessor = new LogEntryProcessor(PendingEntries.createEmpty());
    private AtomicInteger processedLines = new AtomicInteger(0);

    public IOHandler(Scanner scanner, PrintWriter writer) {
        this.scanner = scanner;
        this.writer = writer;
    }

    public void doHandle() {
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (Constants.terminate.equals(line)) {
                break;
            }
            LogEntry logEntry = LogEntryMapper.map(line);
            if (logEntry == null) {
                continue;
            }
            String trace = logEntryProcessor.process(logEntry);
            if (trace != null) {
                writer.println(trace);
                writer.flush();
            }
            processedLines.getAndIncrement();
        }

        while (! logEntryProcessor.finished()) {
            String trace = logEntryProcessor.processPendingEntries();
            if (trace != null) {
                writer.println(trace);
                writer.flush();
            }
        }
    }

    public AtomicInteger getProcessedLines() {
        return processedLines;
    }

    @Override
    public void run() {
        doHandle();
    }
}
