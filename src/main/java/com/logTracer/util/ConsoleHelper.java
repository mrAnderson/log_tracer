package com.logTracer.util;

import java.util.concurrent.atomic.AtomicInteger;

public class ConsoleHelper {

    public void animate(AtomicInteger line) {
        System.out.print("\r" + "Lines processed: " + line);
    }
}