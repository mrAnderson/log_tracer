package com.logTracer.domain;


import org.jetbrains.annotations.Nullable;

import java.time.Instant;
import java.util.Objects;
import java.util.StringJoiner;

public class LogEntry {
    private final String trace;
    private final String serviceName;
    private final String callerSpan;
    private final String span;
    private final Instant startedAt;
    private final Instant endedAt;

    public LogEntry(String trace, String serviceName, @Nullable String callerSpan, String span, Instant startedAt, Instant endedAt) {
        this.trace = trace;
        this.serviceName = serviceName;
        this.callerSpan = callerSpan;
        this.span = span;
        this.startedAt = startedAt;
        this.endedAt = endedAt;
    }

    public String getTrace() {
        return trace;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getCallerSpan() {
        return callerSpan;
    }

    public String getSpan() {
        return span;
    }

    public Instant getStartedAt() {
        return startedAt;
    }

    public Instant getEndedAt() {
        return endedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LogEntry entry = (LogEntry) o;
        return Objects.equals(trace, entry.trace) &&
                Objects.equals(serviceName, entry.serviceName) &&
                Objects.equals(callerSpan, entry.callerSpan) &&
                Objects.equals(span, entry.span) &&
                Objects.equals(startedAt, entry.startedAt) &&
                Objects.equals(endedAt, entry.endedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trace, serviceName, callerSpan, span, startedAt, endedAt);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", LogEntry.class.getSimpleName() + "[", "]")
                .add("trace='" + trace + "'")
                .add("serviceName='" + serviceName + "'")
                .add("callerSpan='" + callerSpan + "'")
                .add("span='" + span + "'")
                .add("startedAt=" + startedAt)
                .add("endedAt=" + endedAt)
                .toString();
    }
}
