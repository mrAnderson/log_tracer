package com.logTracer.domain;

public class Trace {
    private final String id;
    private final Call root;

    public Trace(String id, Call root) {
        this.id = id;
        this.root = root;
    }

    public String getId() {
        return id;
    }

    public Call getRoot() {
        return root;
    }
}
