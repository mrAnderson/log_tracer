package com.logTracer.domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Call {
    private final Instant start;
    private final Instant end;
    private final String service;
    private final String span;
    private List<Call> calls = new ArrayList<>();

    public Call(String span, String service, Instant start, Instant end) {
        this.service = service;
        this.span = span;
        this.start = start;
        this.end = end;
    }

    public void addChild(Call call) {
        calls.add(call);
    }

    public String getService() {
        return service;
    }

    public Instant getStart() {
        return start;
    }

    public Instant getEnd() {
        return end;
    }

    public List<Call> getCalls() {
        return calls;
    }

    public String getSpan() {
        return span;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Call call = (Call) o;
        return Objects.equals(service, call.service) &&
                Objects.equals(span, call.span) &&
                Objects.equals(start, call.start) &&
                Objects.equals(end, call.end) &&
                Objects.equals(calls, call.calls);
    }

    @Override
    public int hashCode() {
        return Objects.hash(service, span, start, end, calls);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Call.class.getSimpleName() + "[", "]")
                .add("service='" + service + "'")
                .add("span='" + span + "'")
                .add("start=" + start)
                .add("end=" + end)
                .add("calls=" + calls)
                .toString();
    }
}
