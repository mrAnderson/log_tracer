package com.logTracer.domain;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class PendingEntries {

    private static final Duration gracePeriod = Duration.ofSeconds(2);

    private final Map<String, List<LogEntry>> groups = new HashMap<>();
    private final Map<String, Instant> latestForGroup = new HashMap<>();
    private final Queue<String> toFree = new LinkedList<>();

    private PendingEntries() {
    }

    public static PendingEntries createEmpty() {
        return new PendingEntries();
    }

    public void add(LogEntry logEntry) {
        String trace = logEntry.getTrace();
        Instant endedAt = logEntry.getEndedAt();
        List<LogEntry> group = groups.get(trace);
        if (group == null) {
            List<LogEntry> entries = new ArrayList<>();
            entries.add(logEntry);
            groups.put(trace, entries);
            toFree.add(trace);
        } else {
            group.add(logEntry);
        }
        latestForGroup.put(trace, endedAt);
    }

    public List<LogEntry> expelByTimeout(LogEntry logEntry) {
        if (toFree.isEmpty()) {
            return Collections.emptyList();
        }
        String peek = toFree.peek();
        if (peek.equals(logEntry.getTrace())) {
            return Collections.emptyList();
        }
        Instant instant = latestForGroup.get(peek);
        if (Duration.between(instant, logEntry.getEndedAt()).abs().compareTo(gracePeriod) > 0) {
            toFree.poll();
            latestForGroup.remove(peek);
            return groups.remove(peek);
        }
        return Collections.emptyList();
    }

    public boolean isEmpty() {
        return groups.isEmpty();
    }

    public List<LogEntry> expel() {
        if (toFree.isEmpty()) {
            return Collections.emptyList();
        }
        String peek = toFree.peek();
        toFree.poll();
        latestForGroup.remove(peek);
        return groups.remove(peek);
    }
}
