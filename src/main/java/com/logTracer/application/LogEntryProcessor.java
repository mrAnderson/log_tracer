package com.logTracer.application;

import com.logTracer.domain.LogEntry;
import com.logTracer.domain.PendingEntries;
import com.logTracer.domain.Trace;

import java.util.List;

public class LogEntryProcessor {
    private final PendingEntries pendingEntries;

    public LogEntryProcessor(PendingEntries pendingEntries) {
        this.pendingEntries = pendingEntries;
    }

    public String processPendingEntries() {
        List<LogEntry> expel = pendingEntries.expel();
        if (expel.isEmpty()) {
            return null;
        }
        return buildTraces(expel);
    }


    public boolean finished() {
        return pendingEntries.isEmpty();
    }

    public String process(LogEntry entry) {
        if (entry == null) {
            return null;
        }
        pendingEntries.add(entry);
        List<LogEntry> logEntries = pendingEntries.expelByTimeout(entry);

        if (logEntries.isEmpty()) {
            return null;
        }
        return buildTraces(logEntries);
    }

    private String buildTraces(List<LogEntry> logEntries) {
        TraceBuilder traceBuilder = new TraceBuilder(logEntries);
        Trace trace = traceBuilder.build();
        if (trace == null) {
            return null;
        }
        return TraceConverter.toJsonString(trace);
    }
}
