package com.logTracer.application;

import com.logTracer.domain.Call;
import com.logTracer.domain.LogEntry;
import com.logTracer.domain.Trace;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TraceBuilder {
    private final List<LogEntry> entries;
    private final Map<String, Call> callIndex = new HashMap<>();

    public TraceBuilder(List<LogEntry> entries) {
        this.entries = entries;
        initIndices();
    }

    private void initIndices() {
        for (LogEntry entry : entries) {
            callIndex.put(entry.getSpan(), new Call(entry.getSpan(), entry.getServiceName(), entry.getStartedAt(), entry.getEndedAt()));
        }
    }

    public Trace build() {
        Call root = null;
        String traceId = null;
        for (LogEntry entry : entries) {
            String callerSpan = entry.getCallerSpan();
            if (callerSpan == null && root != null) {
                return null;
            }
            if (callerSpan == null) {
                root = callIndex.get(entry.getSpan());
                traceId = entry.getTrace();
            } else {
                Call parent = callIndex.get(callerSpan);
                if (parent == null) {
                    return null;
                }
                parent.addChild(callIndex.get(entry.getSpan()));
            }
        }
        return new Trace(traceId, root);
    }
}
