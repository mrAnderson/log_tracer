package com.logTracer.application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.logTracer.domain.Trace;

import java.time.Instant;

public class TraceConverter {
    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(Instant.class, new InstantGsonAdapter())
            .create();

    public static String toJsonString(Trace trace) {
        return gson.toJson(trace);
    }
}
