package com.logTracer;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MainTest {
    private Main main;

    @Before
    public void setup() {
        main = new Main();
    }

    @Test
    public void setupPath_noInput_nullPaths() {
        String[] args = new String[]{};
        main.setupPaths(args);
        assertThat(main.getInputFilePath()).isNull();
        assertThat(main.getOutputFilePath()).isNull();
    }

    @Test
    public void setupPath_inGivenOutNull_inSetOutNull() {
        String input = "input.txt";
        String[] args = new String[]{"-f", input};
        main.setupPaths(args);
        assertThat(main.getInputFilePath()).isEqualTo(input);
        assertThat(main.getOutputFilePath()).isNull();
    }

    @Test
    public void setupPath_outGiven_outSetInNull() {
        String output = "output.txt";
        String[] args = new String[]{"-o", output};
        main.setupPaths(args);
        assertThat(main.getOutputFilePath()).isEqualTo(output);
        assertThat(main.getInputFilePath()).isNull();
    }

    @Test
    public void setupPath_inAndOutGiven_setsPaths() {
        String output = "output.txt";
        String input = "input.txt";
        String[] args = new String[]{"-o", output, "-f", input};

        main.setupPaths(args);
        assertThat(main.getOutputFilePath()).isEqualTo(output);
        assertThat(main.getInputFilePath()).isEqualTo(input);
    }

}