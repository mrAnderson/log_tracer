package com.logTracer.application;

import com.logTracer.domain.Call;
import com.logTracer.domain.Trace;
import org.junit.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

public class TraceConverterTest {

    @Test
    public void toJsonString_singleCall() {
        String traceId = "traceId";
        String span = "span1";
        String service = "service1";
        Instant start = Instant.parse("2019-01-20T14:15:00.271Z");
        Instant end = Instant.parse("2019-01-20T14:15:00.276Z");
        Call entry = new Call(span, service, start, end);
        Trace trace = new Trace(traceId, entry);
        String jsonString = TraceConverter.toJsonString(trace);
        assertThat(jsonString).isEqualTo("{\"id\":\"traceId\",\"root\":{\"start\":\"2019-01-20T14:15:00.271Z\",\"end\":\"2019-01-20T14:15:00.276Z\",\"service\":\"service1\",\"span\":\"span1\",\"calls\":[]}}");
    }

    @Test
    public void toJsonString_callWithChildren() {
        String traceId = "traceId";
        String span = "span1";
        String service = "service1";
        Instant start = Instant.parse("2019-01-20T14:15:00.271Z");
        Instant end = Instant.parse("2019-01-20T14:15:00.276Z");
        Call call1 = new Call(span, service, start, end);
        Trace trace = new Trace(traceId, call1);

        String span2 = "span2";
        String service2 = "service2";
        Instant start2 = Instant.parse("2019-01-20T14:15:01.271Z");
        Instant end2 = Instant.parse("2019-01-20T14:15:01.276Z");
        Call call2 = new Call(span2, service2, start2, end2);

        String span3 = "span3";
        String service3 = "service3";
        Instant start3 = Instant.parse("2019-01-20T14:15:02.271Z");
        Instant end3 = Instant.parse("2019-01-20T14:15:02.276Z");
        Call call3 = new Call(span3, service3, start3, end3);

        call1.addChild(call2);
        call1.addChild(call3);

        String jsonString = TraceConverter.toJsonString(trace);
        System.out.println(jsonString);
        assertThat(jsonString).isEqualTo("{\"id\":\"traceId\",\"root\":{\"start\":\"2019-01-20T14:15:00.271Z\",\"end\":\"2019-01-20T14:15:00.276Z\",\"service\":\"service1\",\"span\":\"span1\",\"calls\":[{\"start\":\"2019-01-20T14:15:01.271Z\",\"end\":\"2019-01-20T14:15:01.276Z\",\"service\":\"service2\",\"span\":\"span2\",\"calls\":[]},{\"start\":\"2019-01-20T14:15:02.271Z\",\"end\":\"2019-01-20T14:15:02.276Z\",\"service\":\"service3\",\"span\":\"span3\",\"calls\":[]}]}}");
    }
}