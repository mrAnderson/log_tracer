package com.logTracer.application;

import com.logTracer.domain.LogEntry;
import com.logTracer.domain.PendingEntries;
import com.logTracer.util.TestUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static com.logTracer.util.TestUtil.randomLogEntry;
import static com.logTracer.util.TestUtil.randomRootLogEntry;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class LogEntryProcessorTest {
    private PendingEntries pendingEntries;
    private LogEntryProcessor logEntryProcessor;

    @Before
    public void setup() {
        pendingEntries = mock(PendingEntries.class);
        logEntryProcessor = new LogEntryProcessor(pendingEntries);
    }

    @Test
    public void finished_noPendingEntries_true() {
        when(pendingEntries.isEmpty()).thenReturn(true);
        assertThat(logEntryProcessor.finished()).isTrue();
    }

    @Test
    public void finished_withPendingEntries_false() {
        when(pendingEntries.isEmpty()).thenReturn(false);
        assertThat(logEntryProcessor.finished()).isFalse();
    }

    @Test
    public void process_nullEntry_null() {
        assertThat(logEntryProcessor.process(null)).isNull();
    }

    @Test
    public void process_ValidEntry_addsToPending_triesToExpelPreviousByTimeout() {
        LogEntry entry = randomLogEntry();
        logEntryProcessor.process(entry);

        verify(pendingEntries).add(entry);
        verify(pendingEntries).expelByTimeout(entry);
    }

    @Test
    public void process_ValidEntry_nothingToExpell() {
        LogEntry entry = randomLogEntry();
        when(pendingEntries.expelByTimeout(entry)).thenReturn(Collections.emptyList());

        assertThat(logEntryProcessor.process(entry)).isNull();
    }

    @Test
    public void process_ValidEntry_readyToExpelByTimeout_previousGroupReturned() {
        LogEntry entry = randomLogEntry();
        List<LogEntry> expelled = Collections.singletonList(TestUtil.randomRootLogEntry());
        when(pendingEntries.expelByTimeout(entry)).thenReturn(expelled);

        assertThat(logEntryProcessor.process(entry)).isNotNull();
    }

    @Test
    public void process_ValidEntry_readyToExpelByTimeout_orphansPending_null() {
        LogEntry entry = randomLogEntry();
        List<LogEntry> expelled = Collections.singletonList(randomLogEntry());
        when(pendingEntries.expelByTimeout(entry)).thenReturn(expelled);

        assertThat(logEntryProcessor.process(entry)).isNull();
    }

    @Test
    public void processPendingEntries_nothingPending_null() {
        when(pendingEntries.expel()).thenReturn(Collections.emptyList());
        assertThat(logEntryProcessor.processPendingEntries()).isNull();
    }

    @Test
    public void processPendingEntries_orphansPending_null() {
        when(pendingEntries.expel()).thenReturn(Collections.singletonList(randomLogEntry()));
        assertThat(logEntryProcessor.processPendingEntries()).isNull();
    }

    @Test
    public void processPendingEntries_validGroupPending_processed() {
        when(pendingEntries.expel()).thenReturn(Collections.singletonList(randomRootLogEntry()));
        assertThat(logEntryProcessor.processPendingEntries()).isNotNull();
    }
}