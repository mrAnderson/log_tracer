package com.logTracer.application;

import com.logTracer.domain.Call;
import com.logTracer.domain.LogEntry;
import com.logTracer.domain.Trace;
import org.junit.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;

import static com.logTracer.util.TestUtil.randomLogEntry;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

public class TraceBuilderTest {

    @Test
    public void buildFromSingleEntry_validTraceWithEntryOnly() {
        LogEntry expected = randomLogEntry(randomAlphabetic(6), null);

        TraceBuilder processor = new TraceBuilder(Collections.singletonList(expected));
        Trace trace = processor.build();

        assertThat(trace.getId()).isEqualTo(expected.getTrace());
        Call entryCall = trace.getRoot();
        assertThat(entryCall.getSpan()).isEqualTo(expected.getSpan());
        assertThat(entryCall.getService()).isEqualTo(expected.getServiceName());
        assertThat(entryCall.getStart()).isEqualTo(expected.getStartedAt());
        assertThat(entryCall.getEnd()).isEqualTo(expected.getEndedAt());
        assertThat(entryCall.getCalls()).isEmpty();
    }

    @Test
    public void buildTrace_multipleValidEntries() {
        String traceId = randomAlphabetic(8);

        LogEntry root = randomLogEntry(traceId, null);
        LogEntry child1 = randomLogEntry(traceId, root.getSpan());
        LogEntry child2 = randomLogEntry(traceId, root.getSpan());

        TraceBuilder processor = new TraceBuilder(Arrays.asList(root, child1, child2));
        Trace trace = processor.build();

        assertThat(trace.getId()).isEqualTo(traceId);
        Call entryCall = trace.getRoot();
        assertThat(entryCall.getSpan()).isEqualTo(root.getSpan());
        assertThat(entryCall.getService()).isEqualTo(root.getServiceName());
        assertThat(entryCall.getStart()).isEqualTo(root.getStartedAt());
        assertThat(entryCall.getEnd()).isEqualTo(root.getEndedAt());
        assertThat(entryCall.getCalls().size()).isEqualTo(2);

        Call actualChild1 = entryCall.getCalls().get(0);
        Call actualChild2 = entryCall.getCalls().get(1);

        assertThat(actualChild1.getSpan()).isEqualTo(child1.getSpan());
        assertThat(actualChild1.getService()).isEqualTo(child1.getServiceName());
        assertThat(actualChild1.getStart()).isEqualTo(child1.getStartedAt());
        assertThat(actualChild1.getEnd()).isEqualTo(child1.getEndedAt());
        assertThat(actualChild1.getCalls().size()).isEqualTo(0);

        assertThat(actualChild2.getSpan()).isEqualTo(child2.getSpan());
        assertThat(actualChild2.getService()).isEqualTo(child2.getServiceName());
        assertThat(actualChild2.getStart()).isEqualTo(child2.getStartedAt());
        assertThat(actualChild2.getEnd()).isEqualTo(child2.getEndedAt());
        assertThat(actualChild2.getCalls().size()).isEqualTo(0);
    }


    @Test
    public void buildFromTwoRoots_cannotProcess() {
        LogEntry root1 = randomLogEntry(randomAlphabetic(6), null);
        LogEntry root2 = randomLogEntry(randomAlphabetic(6), null);

        TraceBuilder processor = new TraceBuilder(Arrays.asList(root1, root2));
        Trace trace = processor.build();

        assertThat(trace).isNull();
    }

    @Test
    public void buildFromNoRoot_cannotProcess() {
        String traceId = randomAlphabetic(6);
        LogEntry root1 = randomLogEntry(traceId, randomAlphabetic(8));
        LogEntry root2 = randomLogEntry(traceId, randomAlphabetic(8));

        TraceBuilder processor = new TraceBuilder(Arrays.asList(root1, root2));
        Trace trace = processor.build();

        assertThat(trace).isNull();
    }

    @Test
    public void buildOrphans_Ignores() {
        String traceId = randomAlphabetic(8);

        LogEntry root = randomLogEntry(traceId, null);
        String child2Span = randomAlphabetic(8);
        LogEntry child1 = randomLogEntry(traceId, child2Span);
        LogEntry child2 = new LogEntry(traceId, randomAlphabetic(6), child1.getSpan(), child2Span, Instant.now(), Instant.now().plusMillis(5));

        TraceBuilder processor = new TraceBuilder(Arrays.asList(root, child1, child2));
        Trace trace = processor.build();

        assertThat(trace.getRoot().getCalls()).isEmpty();
    }
}