package com.logTracer.application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import org.junit.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

public class InstantGsonAdapterTest {

    @Test
    public void serialize() {
        Instant now = Instant.now();
        String expected = now.toString();
        InstantGsonAdapter adapter = new InstantGsonAdapter();
        Gson gson = new GsonBuilder().registerTypeAdapter(Instant.class, adapter).create();
        JsonElement actual = gson.toJsonTree(now, Instant.class);
        assertThat(actual.getAsString()).isEqualTo(expected);
    }
}