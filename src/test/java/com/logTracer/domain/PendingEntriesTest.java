package com.logTracer.domain;

import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.List;

import static com.logTracer.util.TestUtil.randomLogEntry;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

public class PendingEntriesTest {
    private PendingEntries pendingEntries;

    @Before
    public void setup() {
        pendingEntries = PendingEntries.createEmpty();
    }

    @Test
    public void expelByTimeout_noGracePeriodHit_noExpel() {
        Instant firstStart = Instant.now();
        Instant firstEnded = firstStart.plusMillis(2);
        Instant secondStart = Instant.now();
        Instant secondEnded = secondStart.plusMillis(3);
        LogEntry first = randomLogEntry(firstStart, firstEnded);
        pendingEntries.add(first);
        LogEntry second = randomLogEntry(secondStart, secondEnded);
        List<LogEntry> logEntries = pendingEntries.expelByTimeout(second);
        assertThat(logEntries).isEmpty();
    }

    @Test
    public void expelByTimeout_gracePeriodHit_groupExpelled() {
        LogEntry first = randomLogEntry("trace1", randomAlphabetic(8));
        LogEntry second = randomLogEntry("trace1", randomAlphabetic(8));
        pendingEntries.add(first);
        pendingEntries.add(second);
        Instant thirdStartedAt = second.getEndedAt().plusSeconds(2);
        Instant thirdEndedAt = thirdStartedAt.plusMillis(11);
        LogEntry third = randomLogEntry(thirdStartedAt, thirdEndedAt);
        List<LogEntry> logEntries = pendingEntries.expelByTimeout(third);
        assertThat(logEntries).containsOnly(first, second);
    }

    @Test
    public void isEmpty_noPendingEntriesPut_true() {
        assertThat(pendingEntries.isEmpty()).isTrue();
    }

    @Test
    public void isEmpty_entryAdded_false() {
        String trace = randomAlphabetic(8);
        LogEntry entry = randomLogEntry(trace, null);
        pendingEntries.add(entry);
        assertThat(pendingEntries.isEmpty()).isFalse();
    }

    @Test
    public void expel_noPendingEntries_null() {
        assertThat(pendingEntries.expel()).isEmpty();
    }

    @Test
    public void expel_withPendingEntries_expelsFirstAddedEntry() {
        LogEntry entry1 = randomLogEntry();
        LogEntry entry2 = randomLogEntry();
        pendingEntries.add(entry1);
        pendingEntries.add(entry2);
        List<LogEntry> expelled = pendingEntries.expel();
        assertThat(expelled).containsExactly(entry1);
    }
}