package com.logTracer.handler;

import com.logTracer.domain.LogEntry;
import org.junit.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.*;

public class LogEntryMapperTest {
    @Test
    public void validInputString_mapped() {
        String input = "2013-10-23T10:12:38.529Z 2013-10-23T10:12:38.584Z uyq2qizg service8 fi7guxwp->alwamsor";
        LogEntry logEntry = LogEntryMapper.map(input);
        assertThat(logEntry.getTrace()).isEqualTo("uyq2qizg");
        assertThat(logEntry.getServiceName()).isEqualTo("service8");
        assertThat(logEntry.getCallerSpan()).isEqualTo("fi7guxwp");
        assertThat(logEntry.getSpan()).isEqualTo("alwamsor");
        assertThat(logEntry.getStartedAt()).isEqualTo(Instant.parse("2013-10-23T10:12:38.529Z"));
        assertThat(logEntry.getEndedAt()).isEqualTo(Instant.parse("2013-10-23T10:12:38.584Z"));
    }

    @Test
    public void validInputString_nullAsCaller_mapped() {
        String input = "2013-10-23T10:12:38.529Z 2013-10-23T10:12:38.584Z uyq2qizg service8 null->alwamsor";
        LogEntry logEntry = LogEntryMapper.map(input);
        assertThat(logEntry.getTrace()).isEqualTo("uyq2qizg");
        assertThat(logEntry.getServiceName()).isEqualTo("service8");
        assertThat(logEntry.getCallerSpan()).isNull();
        assertThat(logEntry.getSpan()).isEqualTo("alwamsor");
        assertThat(logEntry.getStartedAt()).isEqualTo(Instant.parse("2013-10-23T10:12:38.529Z"));
        assertThat(logEntry.getEndedAt()).isEqualTo(Instant.parse("2013-10-23T10:12:38.584Z"));
    }

    @Test
    public void malformationInSplit_null() {
        String input = "2013-10-23T10:12:38.529Z uyq2qizg service8 fi7guxwp->alwamsor";
        LogEntry logEntry = LogEntryMapper.map(input);
        assertThat(logEntry).isNull();
    }

    @Test
    public void malformationInTimestamp_null() {
        String input = "nut_an_instant 2013-10-23T10:12:38.584Z uyq2qizg service8 fi7guxwp->alwamsor";
        LogEntry logEntry = LogEntryMapper.map(input);
        assertThat(logEntry).isNull();
    }

    @Test
    public void malformationInCallerCallee_null() {
        String input = "2013-10-23T10:12:38.529Z 2013-10-23T10:12:38.584Z uyq2qizg service8 sfafs-alwamsor";
        LogEntry logEntry = LogEntryMapper.map(input);
        assertThat(logEntry).isNull();
    }

}