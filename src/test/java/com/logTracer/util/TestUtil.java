package com.logTracer.util;

import com.logTracer.domain.LogEntry;

import java.time.Instant;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class TestUtil {
    public static LogEntry randomLogEntry(String trace, String callerSpan) {
        Instant startedAt = Instant.now();
        Instant endedAt = Instant.now().plusMillis(5);
        return new LogEntry(trace,
                randomAlphabetic(6),
                callerSpan,
                randomAlphabetic(8),
                startedAt,
                endedAt);
    }

    public static LogEntry randomLogEntry(Instant start, Instant end) {
        return new LogEntry(randomAlphabetic(8),
                randomAlphabetic(6),
                randomAlphabetic(8),
                randomAlphabetic(8),
                start,
                end);
    }

    public static LogEntry randomLogEntry() {
        return new LogEntry(randomAlphabetic(8),
                randomAlphabetic(6),
                randomAlphabetic(8),
                randomAlphabetic(8),
                Instant.now(),
                Instant.now().plusMillis(5));
    }

    public static LogEntry randomRootLogEntry() {
        return new LogEntry(randomAlphabetic(8),
                randomAlphabetic(6),
                null,
                randomAlphabetic(8),
                Instant.now(),
                Instant.now().plusMillis(5));
    }
}
